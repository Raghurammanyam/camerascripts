# -*- mode: python -*-

block_cipher = None


a = Analysis(['campassport.py', 'visionpassport.py', 'countrycodes.py', 'localface.py', 'tkinterform.py'],
             pathex=['/home/raghu/camerascripts'],
             binaries=[],
             datas=[('Har_cascade.xml', '.')],
             hiddenimports=['_strptime', 'cv2'],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          [],
          exclude_binaries=True,
          name='campassport',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          console=True )
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=True,
               name='campassport')
