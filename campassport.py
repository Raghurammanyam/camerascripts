import cv2
import base64
from visionpassport import detect_text
from localface import detect_faces
from tkinterform import form
import os
from datetime import datetime,date
from os.path import expanduser
import logging
import logging.handlers
import traceback
import sys
home= expanduser('~')
LOG_FILENAME = home+'/'+'detail.log'
logging.basicConfig(
    level=logging.DEBUG,
    format="%(asctime)s - %(levelname)s - %(message)s",
    handlers=[
        logging.handlers.RotatingFileHandler(LOG_FILENAME, maxBytes=30000000, backupCount=100),
        logging.StreamHandler(sys.stdout)
    ])
cam_logger = logging.getLogger('detailslogger')
try:
        
    face_path=home +'/'+'xxxx'+str(datetime.now().time())+'face.jpeg'
    image_path=home+'/'+str(datetime.now().time())+'opencv.jpeg'
    cap = cv2.VideoCapture(0)
    cam_logger.info("camera initiated")
    retval, image = cap.read()
    retval, buffer = cv2.imencode('.jpg', image)
    # cv2.imshow('image',image)
    cv2.waitKey(20000)
    jpg_as_text = base64.b64encode(buffer).decode()
    cam_logger.info("buffer content:{}".format(jpg_as_text[0:10]))
    cv2.imwrite(image_path, image)
    # print(jpg_as_text)
    cap.release()
    details =detect_text(jpg_as_text)
    cam_logger.info("camera_details_content:{}".format(details))
    if 'error' in details.keys():
        details={'IDType': ' ', 'PassportNumber': ' ', 'FirstName': ' ', 'MiddleName': ' ', 'LastName': ' ', 'Sex': ' ', 'BirthDate': ' ', 'PlaceOfBirth': ' ', 'Country': ' ', 'CountryLong': ' ', 'Nationality': ' ', 'NationalityLong': ' ', 'ExpirationDate': ' ', 'Address1': ' ', 'Address2': ' ', 'Address3': ' ', 'IssueDate': ' ', 'PersonalNumber': ' '}
    elif 'error' not in details.keys():
        details=details
    cam_logger.info("details:{}".format(details))
    #print(xml = dicttoxml(details))
    face =detect_faces(image_path,face_path)
    image_string = ' '
    if os.path.isfile(face) is True:
        face=face
        with open(face, 'rb') as image:
            image_string = base64.b64encode(image.read()).decode()
    form(image_path,face_path,details)
except Exception as e:
    cam_logger.warning(traceback.format_exc())


