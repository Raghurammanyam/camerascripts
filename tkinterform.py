import tkinter as tk
from PIL import Image,ImageTk
from dicttoxml import dicttoxml
import base64
import sys
from os.path import expanduser
import logging
import logging.handlers
import traceback
home=expanduser('~')
LOG_FILENAME = home+'/'+'detail.log'
logging.basicConfig(
    level=logging.DEBUG,
    format="%(asctime)s - %(levelname)s - %(message)s",
    handlers=[
        logging.handlers.RotatingFileHandler(LOG_FILENAME, maxBytes=30000000, backupCount=100),
        logging.StreamHandler(sys.stdout)
    ])
form_logger = logging.getLogger('detailslogger')
def form(image,face,details):
    try:
        form_logger.info("form_initiated")
        imag = image
        im=Image.open(imag)
        new_img = im.resize((512,256))
        new_img.save(home+'/document.ppm')
        #face =detect_faces('/home/raghu/rotated.jpeg','/home/raghu/facedd.jpeg')
        "/////////////////////////////////////////////////////////"
        im1=Image.open(face)
        new_img1 = im1.resize((256,256))
        new_img1.save(home+'/face.ppm')
        # with open(imag, 'rb') as image:
        #     image_string = base64.b64encode(image.read()).decode()
    # details=detect_text(image_string)
        print("details:",details)
        # if 'error' in details.keys():
        #     details={'IDType': ' ', 'PassportNumber': ' ', 'FirstName': ' ', 'MiddleName': ' ', 'LastName': ' ', 'Sex': ' ', 'BirthDate': ' ', 'PlaceOfBirth': ' ', 'Country': ' ', 'CountryLong': ' ', 'Nationality': ' ', 'NationalityLong': ' ', 'ExpirationDate': ' ', 'Address1': ' ', 'Address2': ' ', 'Address3': ' ', 'IssueDate': ' ', 'PersonalNumber': ' '}
        # elif 'error' not in details.keys():
        #     details=details
        master = tk.Tk()
        # master.geometry("1080x540")
        img_label = tk.Label(master)
        img_label.image = tk.PhotoImage(file="/home/raghu/document.ppm")
        img_label['image'] = img_label.image
        img_label.grid(row=0,columnspan=4)
        "........................................................................."
        img_label1 = tk.Label(master)
        img_label1.image = tk.PhotoImage(file="/home/raghu/face.ppm")
        img_label1['image'] = img_label1.image
        img_label1.grid(row=0,columnspan=14,sticky=tk.E)

        #details={"IDType":"passport","PassportNumber":"INNG778893","FirstName":"ram","MiddleName":"","LastName":"raghu","Sex":"M","BirthDate":"23/09/1994","PlaceOfBirth":"","Country":"IND","CountryLong":"India","Nationality":"IND","NationalityLong":"India","ExpirationDate":"23/09/2030","Address1":"","Address2":"","Address3":"","IssueDate":"23/09/2018","PersonalNumber":""}
        #tk.Label(master,image="/home/raghu/document.ppm").grid(row=0)
        # print(dicttoxml(details))
        tk.Label(master, text="IDType: ").grid(row=1,column=0, sticky=tk.W, pady=10)
        tk.Label(master, text="Passport/VisaNumber:").grid(row=1,column=2, sticky=tk.W, pady=10)
        tk.Label(master,text="FirstName: ").grid(row=2,column=0,sticky=tk.W, pady=10)
        tk.Label(master,text="LastName: ").grid(row=2,column=2,sticky=tk.W, pady=10)
        tk.Label(master,text="Country: ").grid(row=3,column=0,sticky=tk.W, pady=10)
        tk.Label(master,text="Nationality: ").grid(row=3,column=2,sticky=tk.W, pady=10)
        tk.Label(master,text="MiddleName:").grid(row=4,column=0,sticky=tk.W,pady=10)
        tk.Label(master,text="Sex:").grid(row=4,column=2,sticky=tk.W,pady=10)
        tk.Label(master,text="BirthDate:").grid(row=5,column=0,sticky=tk.W,pady=10)
        tk.Label(master,text="PlaceOfBirth:").grid(row=5,column=2,sticky=tk.W,pady=10)
        tk.Label(master,text="NationalityLong:").grid(row=6,column=0,sticky=tk.W,pady=10)
        tk.Label(master,text="CountryLong:").grid(row=6,column=2,sticky=tk.W,pady=10)
        tk.Label(master,text="ExpirationDate:").grid(row=7,column=0,sticky=tk.W,pady=10)
        tk.Label(master,text="Address1:").grid(row=7,column=2,sticky=tk.W,pady=10)
        tk.Label(master,text="Address2:").grid(row=1,column=5,sticky=tk.E,pady=10)
        tk.Label(master,text="Address3:").grid(row=2,column=5,sticky=tk.E,pady=10)
        tk.Label(master,text="IssueDate:").grid(row=3,column=5,sticky=tk.E,pady=10)
        tk.Label(master,text="PersonalNumber:").grid(row=4,column=5,sticky=tk.E,pady=10)
        tk.Button(master, text="OK",fg="Black", command=lambda:[update(),master.quit()]).grid(row=10,columnspan=8,padx=5,pady=10)
        e1 = tk.Entry(master)
        e2 = tk.Entry(master)
        e3 =tk.Entry(master)
        e4=tk.Entry(master)
        e5=tk.Entry(master)
        e6=tk.Entry(master)
        e7=tk.Entry(master)
        e8=tk.Entry(master)
        e9=tk.Entry(master)
        e10=tk.Entry(master)
        e11=tk.Entry(master)
        e12=tk.Entry(master)
        e13=tk.Entry(master)
        e14=tk.Entry(master)
        e15=tk.Entry(master)
        e16=tk.Entry(master)
        e17=tk.Entry(master)
        e18=tk.Entry(master)
        e1.grid(row=1, column=1,sticky=tk.W, pady=10)
        e2.grid(row=1, column=3,sticky=tk.W, pady=10)
        e3.grid(row=2,column=1,sticky=tk.W,pady=10)
        e4.grid(row=2,column=3,sticky=tk.W,pady=10)
        e5.grid(row=3,column=1,sticky=tk.W,pady=10)
        e6.grid(row=3,column=3,sticky=tk.W,pady=10)
        e7.grid(row=4,column=1,sticky=tk.W,pady=10)
        e8.grid(row=4,column=3,sticky=tk.W,pady=10)
        e9.grid(row=5,column=1,sticky=tk.W,pady=10)
        e10.grid(row=5,column=3,sticky=tk.W,pady=10)
        e11.grid(row=6,column=1,sticky=tk.W,pady=10)
        e12.grid(row=6,column=3,sticky=tk.W,pady=10)
        e13.grid(row=7,column=1,sticky=tk.W,pady=10)
        e14.grid(row=7,column=3,sticky=tk.W,pady=10)
        e15.grid(row=1,column=6,sticky=tk.W,pady=10)
        e16.grid(row=2,column=6,sticky=tk.W,pady=10)
        e17.grid(row=3,column=6,sticky=tk.W,pady=10)
        e18.grid(row=4,column=6,sticky=tk.W,pady=10)
        e1.insert(15, details['IDType'])
        e2.insert(10, details['PassportNumber'])
        e3.insert(10,details['FirstName'])
        e4.insert(10,details['LastName'])
        e5.insert(10,details['Country'])
        e6.insert(10,details['Nationality'])
        e7.insert(10,details['MiddleName'])
        e8.insert(10,details['Sex'])
        e9.insert(10,details['BirthDate'])
        e10.insert(10,details['PlaceOfBirth'])
        e11.insert(10,details['NationalityLong'])
        e12.insert(10,details['CountryLong'])
        e13.insert(10,details['ExpirationDate'])
        e14.insert(10,details['Address1'])
        e15.insert(10,details['Address2'])
        e16.insert(10,details['Address3'])
        e17.insert(10,details['IssueDate'])
        e18.insert(10,details['PersonalNumber'])
        print(details)
        print("...........................................")
        def update():
            details={"IDType":str(e1.get()),"PassportNumber":str(e2.get()),"FirstName":str(e3.get()),"MiddleName":str(e7.get()),"LastName":str(e4.get()),"Sex":str(e8.get()),"BirthDate":str(e9.get()),"PlaceOfBirth":str(e10.get()),"Country":str(e5.get()),"CountryLong":str(e12.get()),"Nationality":str(e6.get()),"NationalityLong":str(e11.get()),"ExpirationDate":str(e13.get()),"Address1":str(e14.get()),"Address2":str(e15.get()),"Address3":str(e16.get()),"IssueDate":str(e17.get()),"PersonalNumber":str(e18.get())}
            form_logger.info("details from tkinter form :{}".format(details))
        
        
            # empty={key:'' for key,value in details.items()}
            # print("empty:",empty)
            
            alpha='<'+'IDType'+'>'+str(details['IDType'])+';'+'<'+'PassportNumber'+'>'+str(details['PassportNumber'])+';'+'<'+'FirstName'+'>'+str(details['FirstName'])+';'+'<'+'MiddleName'+'>'+str(details['MiddleName'])+';'+'<'+'LastName'+'>'+str(details['LastName'])+';'+'<'+'Sex'+'>'+str(details['Sex'])+';'+'<'+'BirthDate'+'>'+str(details['BirthDate'])+';'+'<'+'PlaceOfBirth'+'>'+str(details['PlaceOfBirth'])+';'+'<'+'Country'+'>'+str(details['Country'])+';'+'<'+'CountryLong'+'>'+str(details['CountryLong'])+';'+'<'+'Nationality'+'>'+str(details['Nationality'])+';'+'<'+'NationalityLong'+'>'+str(details['NationalityLong'])+';'+'<'+'ExpirationDate'+'>'+str(details['ExpirationDate'])+';'+'<'+'Address1'+'>'+str(details['Address1'])+';'+'<'+'Address2'+'>'+str(details['Address2'])+';'+'<'+'Address3'+'>'+str(details['Address3'])+';'+'<'+'IssueDate'+'>'+str(details['IssueDate'])+';'+'<'+'PersonalNumber'+'>'+str(details['PersonalNumber'])+';'
            with open (home+"/xml.txt",'w') as f:
                f.write(str(alpha))
                form_logger.info("data written into txt file")
            return details
        master.after(2000, master.focus_force)
        master.mainloop()
    except Exception as e:
        form_logger.warning(traceback.format_exc())